import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { EmailEntity } from "./email.entity";
import { EmailId } from "./email.interfaces";
import { AddEmail } from "./email.types";
import { UserEntity } from "../user/user.entity";

@Injectable()
export class EmailService {
	constructor(
		@InjectRepository(EmailEntity)
		private readonly emailRepository: Repository<EmailEntity>,
		@InjectRepository(UserEntity)
		private readonly userRepository: Repository<UserEntity>
	) {}
	
	async add(email: AddEmail) {
		if(
			(await (this.userRepository.findOneBy({ id: email.userId}))).status === 'active'
		)
			return (
				await this.emailRepository.insert(email)
			).identifiers[0].id;
		
		throw new Error("Les adresses e-mails d\'un utilisateur inactif ne peut être modifiés")
	}
	
	async delete(id: string) {
		const email = await this.emailRepository.findOneBy({id})
		if(
			(await (this.userRepository.findOneBy({ id: email.userId}))).status === 'active'
		)
			return (
				await this.emailRepository.delete(id)
			).affected
		
		throw new Error("Les adresses e-mails d\'un utilisateur inactif ne peut être modifiés")
	}
	
	async get(id: EmailId) {
		return this.emailRepository.findOneBy({ id: id });
	}

	async find(options: FindManyOptions<EmailEntity>) {
		return this.emailRepository.find(options)
	}
}
import {
  Args,
  ID, Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { AddEmail, EmailFiltersArgs, UserEmail } from './email.types';
import { User } from '../user/user.types';
import { EmailService } from "./email.service";
import { Equal, FindManyOptions, FindOptionsWhere, In, Repository } from "typeorm";
import { EmailEntity } from "./email.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { UserEntity } from "../user/user.entity";
import { EmailId } from "./email.interfaces";

@Resolver(() => UserEmail)
export class EmailResolver {
  constructor(
    private readonly _service: EmailService,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>
  ) {
  }
  
  @Query(() => UserEmail, { name: 'email' })
  getEmail(@Args({ name: 'emailId', type: () => ID }) emailId: string) {
    return this._service.get(emailId);
  }
  
  @Query(() => [UserEmail], { name: 'emailsList' })
  async getEmails(@Args() filters: EmailFiltersArgs): Promise<UserEmail[]> {
    const whereAddressEqual: FindOptionsWhere<EmailEntity> = filters.address && filters.address.equal
      ? {address: Equal(filters.address.equal)} : {}
    
    const whereAddressIn: FindOptionsWhere<EmailEntity> = filters.address && filters.address.in
      ? {address: In(filters.address.in)} : {}
    
    const Where: FindManyOptions<EmailEntity> = {
      where: [whereAddressEqual, whereAddressIn],
      order: {address: 'asc'}
    }
    
    return this._service.find(Where);
  }
  
  @Mutation(() => ID)
  async addEmail(@Args() email: AddEmail): Promise<EmailId> {
    return this._service.add(email);
  }
  
  @Mutation(() => ID)
  deleteEmail(@Args({name: 'emailId', type: () => ID}) emailId): Promise<number> {
    return this._service.delete(emailId);
  }
  
  @ResolveField(() => User, { name: 'user' })
  async getUser(@Parent() parent: UserEmail): Promise<User> {
    return this.userRepository.findOneBy({
      id: parent.userId
    });
  }
}
